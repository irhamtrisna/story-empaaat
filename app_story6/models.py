from django.db import models

# Create your models here.

class Kegiatan(models.Model):
    Nama_Kegiatan = models.CharField(max_length=50)
    def __str__(self):
        return self.nama

class Peserta(models.Model):
    Nama_Peserta = models.CharField(max_length=50)
    ikutKegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, null=True, blank=True)