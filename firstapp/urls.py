from django.urls import path

from . import views

app_name = 'firstapp'

urlpatterns = [
    path('', views.home, name='home'),
    path('about', views.about, name='about'),
    path('achievements', views.achievements, name='achievements'),
    path('time/<str:timezone>', views.time),
    path('schedule/', views.indexjadwal, name='indexjadwal'),
    path('inputSchedule', views.schedule, name='schedule'),
    path('viewSchedule/<int:pk>', views.viewSchedule, name='viewSchedule'),
    path('delete/<str:mataKuliah>', views.delete, name='delete')
]