from django.shortcuts import render, redirect

# Create your views here.
from django.http import HttpResponse
import datetime
from .models import Schedule
from .forms import ScheduleForm



def home(request):
    return render(request,'landing-page.html')

def about(request):
    return render(request, 'index.html')

def achievements(request):
    return render(request, 'prestasi.html')

def time(request, timezone="0"):
    time = datetime.datetime.now() + datetime.timedelta(hours=int(timezone)+7)
    return render(request, 'time.html', {'time':time})

def indexjadwal(request):
    matkul = Schedule.objects.all()
    context = {
        'object' : matkul
    }
    return render(request, 'indexjadwal.html', context)


def schedule(request):
    if request.method == "POST":
        form = ScheduleForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('firstapp:indexjadwal')
    
    else:
        form = ScheduleForm()

    return render(request, 'jadwal.html', {'form':form})

def delete(request, mataKuliah):
    matKul = Schedule.objects.filter(mataKuliah = mataKuliah)
    matKul.delete()
    return redirect('firstapp:schedule')

def viewSchedule(request, pk):
    matkul = Schedule.objects.get(id=pk)
    return render(request, 'viewSchedule.html', {'x' : matkul})
