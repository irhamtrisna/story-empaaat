from django.db import models

# Create your models here.

class Schedule(models.Model):
    mataKuliah = models.CharField(max_length=50)
    dosenPengajar = models.CharField(max_length=25)
    jumlahSKS = models.CharField(max_length=4)
    semesterTahun = models.CharField(max_length=20)
    ruang = models.CharField(max_length=10)
    deskripsi = models.CharField(max_length=100)

    def __str__(self):
        return self.mataKuliah